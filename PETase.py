#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 29 15:30:33 2019

@author: qtbstudent
"""

import modelbase
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sb

p={'MHET_ext':1,'k_rev':0.2,'suc_eff':0.1,'acoa_eff':0.1,'KcPETase':27,'KmPETase':4600,'KcMHETase':31,'KmMHETase':7.3,'KcTPADO':22.043,'KmTPADO_TPA':72,'KmTPADO_NADH':51,'KcDCDDH':20.7,'KmDCDDH_dd':90,'KmDCDDH_NAD':43,'KcPCD':70,'KmPCD':17,'KcCMLE':600,'KmCMLE':250,'KCMD':820,'KcKELH':131.45,'KmKELH':12,'KcOSCT':23.83,'KmOSCT_O':400,'KmOSCT_S':200,'KcOCT':7.83,'KmOCT_O':150,'KmOCT_C':10,'PETase':1.0,'MHETase':1.0,'TPADO':1.0,'DCDDH':1.0,'PCD':1.0,'CMLE':1.0,'KELH':1.0,'OSCT':1.0,'OCT':1.0}
m=modelbase.Model(p)

metabolites=['MHET','TPA','DCDD','PCA','CM','CDOA','DOA','OXO','OCOA','ACOA','SUC','SCOA','NADH','NAD']
m.set_cpds(metabolites)


def acoa_eff(p,ACOA):
     return p.acoa_eff*ACOA
m.set_rate('acoa_eff',acoa_eff,'ACOA')
m.set_stoichiometry('acoa_eff',{'ACOA':-1})

def suc_eff(p,SUC):
     return p.suc_eff*SUC
m.set_rate('suc_eff',suc_eff,'SUC')
m.set_stoichiometry('suc_eff',{'SUC':-1})

def MHET_transport(p,MHET):
     return (p.k_rev*p.MHET_ext)-(p.k_rev*MHET)
m.set_rate('MHET_transport',MHET_transport,'MHET')

def Vmhetase(p,MHET):
     return  (p.KcMHETase*MHET)/(p.KmMHETase+MHET)
m.set_rate('Vmhetase',Vmhetase,'MHET')

def Vtpado(p,TPA,NADH):
     return (p.KcTPADO*TPA*NADH)/(p.KmTPADO_TPA+TPA+p.KmTPADO_NADH+NADH)
m.set_rate('Vtpado',Vtpado,'TPA','NADH')

def Vdcddh(p,DCDD,NAD):
     return (p.KcDCDDH*DCDD*NAD)/(p.KmDCDDH_dd+DCDD+p.KmDCDDH_NAD+NAD)
m.set_rate('Vdcddh',Vdcddh,'DCDD','NAD')

def Vpcd(p,PCA):
     return  (p.KcPCD*PCA)/(p.KmPCD+PCA)
m.set_rate('Vpcd',Vpcd,'PCA')

def Vcmle(p,CM):
     return  (p.KcCMLE*CM)/(p.KmCMLE+CM)
m.set_rate('Vcmle',Vcmle,'CM')

def Vcdoa(p,CDOA):
     return  p.KCMD*CDOA
m.set_rate('Vcdoa',Vcdoa,'CDOA')

def Vkelh(p,DOA):
     return  (p.KcKELH*DOA)/(p.KmKELH+DOA)
m.set_rate('Vkelh',Vkelh,'DOA')

def Vosct(p,OXO,SCOA):
     return  (p.KcOSCT*OXO*SCOA)/(p.KmOSCT_O+OXO+p.KmOSCT_S+SCOA)
m.set_rate('Vosct',Vosct,'OXO','SCOA')

def Voct(p,OCOA):
     return  (p.KcOCT*OCOA)/(p.KmOCT_O+OCOA+p.KmOCT_C)
m.set_rate('Voct',Voct,'OCOA')


m.set_stoichiometry('MHET_transport',{'MHET':1})
m.set_stoichiometry('Vmhetase',{'MHET':-1,'TPA':1})
m.set_stoichiometry('Vtpado',{'TPA':-1,'NADH':-1,'DCDD':1,'NAD':1})
m.set_stoichiometry('Vdcddh',{'DCDD':-1,'NAD':-1,'PCA':1,'NADH':1})
m.set_stoichiometry('Vpcd',{'PCA':-1,'CM':1})
m.set_stoichiometry('Vcmle',{'CM':-1,'CDOA':1})
m.set_stoichiometry('Vcdoa',{'CDOA':-1,'DOA':1})
m.set_stoichiometry('Vkelh',{'DOA':-1,'OXO':1})
m.set_stoichiometry('Vosct',{'OXO':-1,'SCOA':-1,'OCOA':1,'SUC':1})
m.set_stoichiometry('Voct',{'OCOA':-1,'ACOA':1,'SCOA':1})

s=modelbase.Simulator(m)
T=np.linspace(0.,1000.,1000)
initial_values=[0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,10.0,10.0,0.]
s.timeCourse(T,initial_values)

results=s.getY()
result=s.sim2SteadyState(initial_values)
plt.plot(T,results[:,0],'b--',label='MHET')
plt.plot(T,results[:,1],label='TPA')
plt.plot(T,results[:,2],label='DCDD')
plt.plot(T,results[:,3],label='PCA')
plt.plot(T,results[:,4],label='CM')
plt.plot(T,results[:,5],label='CDOA')
plt.plot(T,results[:,6],label='DOA')
plt.plot(T,results[:,7],label='OXO')
plt.plot(T,results[:,8],label='OCOA')
plt.plot(T,results[:,9],'y--',label='ACOA')
plt.plot(T,results[:,10],'r--',label='SUC')
plt.plot(T,results[:,11],label='SCOA')
plt.plot(T,results[:,12],label='NADH')
plt.plot(T,results[:,13],label='NAD')
plt.legend(loc='best')
plt.title('Timecourse simulation of metabolites concentration in PET degration')
plt.xlabel('Time')
plt.ylabel('Concentration of substrate')
plt.savefig('ss.png')
plt.show()

substrates={
        "MHET":0,
        "TPA":1,
        "DCDD":2,
        'PCA':3,
        'CM':4,
        'CDOA':5,
        'DOA':6,
        'OXO':7,
        'OCOA':8,
        'ACOA':9,
        'SUC':10,
        'SCOA':11,
        'NADH':12,
        'NAD':13
        }

#Calculate concentration control coefficient

def cc1(m,s,init,par,var):


     T=np.linspace(0.,10000,1000)
     or_dic=m.par.__dict__
     Y=dict()
     X=dict()
     s.timeCourse(T,init)
     for name in m.rateNames():
         X.update({name:s.getRate(name)[-1]})
     for name in m.cpdNames:
         Y.update({name:s.getVarByName(name)[-1]})
     s.clearResults()
     Y1=dict()
     X1=dict()
     xvar=or_dic[par]
     s.model.par.update({par:xvar*(1+var)})
     s.timeCourse(T,init)
     for name in m.rateNames():
         X1.update({name:s.getRate(name)[-1]})
     for name in m.cpdNames:
         Y1.update({name:s.getVarByName(name)[-1]})
     s.clearResults()
     Y99=dict()
     X99=dict()
     xvar=or_dic[par]
     s.model.par.update({par:xvar*(1-var)})
     s.timeCourse(T,init)
     for name in m.rateNames():
         X99.update({name:s.getRate(name)[-1]})
     for name in m.cpdNames:
         Y99.update({name:s.getVarByName(name)[-1]})
     s.clearResults()
     s.model.par.update({par:xvar})
     cc_dict={}


     for i in list(m.cpdNames):
         cc_now= ((Y1[i]-Y99[i]) / Y[i])/(var*2)

         cc_dict.update({i:cc_now})
     return cc_dict

v_maxes=['k_rev','suc_eff','acoa_eff','KcPETase','KcMHETase','KcTPADO','KcDCDDH','KcPCD','KcCMLE','KCMD','KcKELH','KcOSCT','KcOCT']
cc_dict={}
conc_control=[]
mat=[]

for i in v_maxes:
    cc_dict.update({i:cc1(m,s,initial_values,i,0.01)})
     

for i in list(metabolites):
    x=0
    for j in list(cc_dict):
        x+=cc_dict[j][i]
        mat.append(cc_dict[j][i])
    conc_control.append(mat)
    mat=[]
    print(i,x)

fig2,ax2 = plt.subplots(figsize=(15,15))
conc=['MHET','TPA','DCDD','PCA','CM','CDOA','DOA','OXO','OCOA','ACOA','SUC','SCOA','NADH','NAD']
enzymes=['k_rev','suc_eff','acoa_eff','KcPETase','KcMHETase','KcTPADO','KcDCDDH','KcPCD','KcCMLE','KCMD','KcKELH','KcOSCT','KcOCT']
sb.heatmap(conc_control,annot=True,cmap="BuPu")
ax2.set_xticklabels(enzymes)
ax2.set_yticklabels(conc)
ax2.set_title('Heatmap of concentration control vs enzyme')
plt.savefig('CC.png')
 

#Systematic change of parameters 
def sys_change(m,var,init,par):
    re=[]
    or_dic=m.par.__dict__
    xvar=or_dic[par]

    for i in np.linspace(-90,90,10):
        s.model.par.update({par:xvar*(1+i*var)})
        ss=s.sim2SteadyState(init)
        re.append(ss)    
    s.model.par.update({par:xvar})
    return np.array(re)


x=np.linspace(0.9,1.9,10)
for j in v_maxes:
    res=sys_change(m,0.01,initial_values,j)
    fig1,ax1 = plt.subplots(figsize=(15,15))
    for i in range(len(metabolites)):
        plt.plot(x,res[:,i],label=metabolites[i])
    plt.title(j)
    plt.legend(loc="best")
    plt.xlabel('Change in parametre')
    plt.ylabel('Concentration of steady-state substrate')
    plt.show()
    plt.savefig('j.png')